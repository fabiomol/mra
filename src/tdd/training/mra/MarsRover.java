package tdd.training.mra;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class MarsRover {
	
	private int planetX, planetY, roverPositionX, roverPositionY;
	
	protected List<String> planetObstacles;
	
	private enum Facing {
		N,
		S,
		E,
		W
	}
	
	private Facing facing;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		
		this.planetX = planetX - 1;
		this.planetY = planetY - 1;
		this.planetObstacles = planetObstacles;
		
		facing = facing.N;
		roverPositionX = 0;
		roverPositionY = 0;
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		
		for(String string : planetObstacles) {
			if(x == Integer.parseInt(string.substring(1,2)) && y == Integer.parseInt(string.substring(3, 4))) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		String obstacle = "";
		
		for(char c : commandString.toCharArray()) {
			if(c == 'l') {
				switch (facing){
					case N:
						facing = facing.W;
						break;
					case W:
						facing = facing.S;
						break;
					case S:
						facing = facing.E;
						break;
					case E:
						facing = facing.N;
						break;
				}
			}
			
			if(c == 'r') {
				switch (facing){
					case N:
						facing = facing.E;
						break;
					case E:
						facing = facing.S;
						break;
					case S:
						facing = facing.W;
						break;
					case W:
						facing = facing.N;
						break;
				}
			}
			
			if(c == 'f') {
				if(facing == facing.N) {
					roverPositionY++;
					checkborder();
					if(planetContainsObstacleAt(roverPositionX, roverPositionY)) {
						if(!obstacle.contains("(" +  roverPositionX + "," + roverPositionY + ")")) {
							obstacle = obstacle + "(" +  roverPositionX + "," + roverPositionY + ")";
						}
						roverPositionY--;
					}
					checkborder();
				}
				if(facing == facing.S) {
					roverPositionY--;
					checkborder();
					if(planetContainsObstacleAt(roverPositionX, roverPositionY)) {
						if(!obstacle.contains("(" +  roverPositionX + "," + roverPositionY + ")")) {
							obstacle = obstacle + "(" +  roverPositionX + "," + roverPositionY + ")";
						}
						roverPositionY++;
					}
					checkborder();
				}
				if(facing == facing.E) {
					roverPositionX++;
					checkborder();
					if(planetContainsObstacleAt(roverPositionX, roverPositionY)) {
						if(!obstacle.contains("(" +  roverPositionX + "," + roverPositionY + ")")) {
							obstacle = obstacle + "(" +  roverPositionX + "," + roverPositionY + ")";
						}
						roverPositionX--;
					}
					checkborder();
				}
				if(facing == facing.W) {
					roverPositionX--;
					checkborder();
					if(planetContainsObstacleAt(roverPositionX, roverPositionY)) {
						if(!obstacle.contains("(" +  roverPositionX + "," + roverPositionY + ")")) {
							obstacle = obstacle + "(" +  roverPositionX + "," + roverPositionY + ")";
						}
						roverPositionX++;
					}
					checkborder();
				}
			}
			
			if(c == 'b') {
				if(facing == facing.N) {
					roverPositionY--;
					checkborder();
					if(planetContainsObstacleAt(roverPositionX, roverPositionY)) {
						if(!obstacle.contains("(" +  roverPositionX + "," + roverPositionY + ")")) {
							obstacle = obstacle + "(" +  roverPositionX + "," + roverPositionY + ")";
						}
						roverPositionY++;
					}
					checkborder();
				}
				if(facing == facing.S) {
					roverPositionY++;
					checkborder();
					if(planetContainsObstacleAt(roverPositionX, roverPositionY)) {
						if(!obstacle.contains("(" +  roverPositionX + "," + roverPositionY + ")")) {
							obstacle = obstacle + "(" +  roverPositionX + "," + roverPositionY + ")";
						}
						roverPositionY--;
					}
					checkborder();
				}
				if(facing == facing.E) {
					roverPositionX--;
					checkborder();
					if(planetContainsObstacleAt(roverPositionX, roverPositionY)) {
						if(!obstacle.contains("(" +  roverPositionX + "," + roverPositionY + ")")) {
							obstacle = obstacle + "(" +  roverPositionX + "," + roverPositionY + ")";
						}
						roverPositionX++;
					}
					checkborder();
				}
				if(facing == facing.W) {
					roverPositionX++;
					checkborder();
					if(planetContainsObstacleAt(roverPositionX, roverPositionY)) {
						if(!obstacle.contains("(" +  roverPositionX + "," + roverPositionY + ")")) {
							obstacle = obstacle + "(" +  roverPositionX + "," + roverPositionY + ")";
						}
						roverPositionX--;
					}
					checkborder();
				}
			}
		}
		
		return "(" + roverPositionX + "," + roverPositionY + "," + facing.toString() + ")" + obstacle;
	}
	
	private void checkborder() {
		if(roverPositionX < 0) {
			roverPositionX = planetX;
		}
		if(roverPositionX > planetX) {
			roverPositionX = 0;
		}
		if(roverPositionY < 0) {
			roverPositionY = planetY;
		}
		if(roverPositionY > planetY) {
			roverPositionY = 0;
		}
	}

}
