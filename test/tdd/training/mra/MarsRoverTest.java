package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void test1() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(4,7)");
		listObstacle.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		boolean test = rover.planetContainsObstacleAt(2, 3);
		assert true == test;
		
		test = rover.planetContainsObstacleAt(5, 6);
		assert false == test;
	}
	
	@Test
	public void test2() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(4,7)");
		listObstacle.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		String string = rover.executeCommand("");
		assertEquals("(0,0,N)", string);
	}
	
	@Test
	public void test3() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(4,7)");
		listObstacle.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		String string = rover.executeCommand("l");
		assertEquals("(0,0,W)", string);
		
		string = rover.executeCommand("r");
		assertEquals("(0,0,N)", string);
	}

	
	@Test
	public void test4() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(4,7)");
		listObstacle.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		String string = rover.executeCommand("f");
		assertEquals("(0,1,N)", string);
	}
	
	
	@Test
	public void test5() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(4,7)");
		listObstacle.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		String string = rover.executeCommand("f");
		assertEquals("(0,1,N)", string);
		
		string = rover.executeCommand("b");
		assertEquals("(0,0,N)", string);
	}
	
	@Test
	public void test6() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(4,7)");
		listObstacle.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		String string = rover.executeCommand("ffrff");
		assertEquals("(2,2,E)", string);
	}
	
	@Test
	public void test7() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(4,7)");
		listObstacle.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		String string = rover.executeCommand("b");
		assertEquals("(0,9,N)", string);
	}
	
	@Test
	public void test8() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(4,7)");
		listObstacle.add("(2,2)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		String string = rover.executeCommand("ffrfff");
		assertEquals("(1,2,E)(2,2)", string);
	}
	
	@Test
	public void test9() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(2,1)");
		listObstacle.add("(2,2)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		String string = rover.executeCommand("ffrfffrflf");
		assertEquals("(1,1,E)(2,2)(2,1)", string);
	}
	
	@Test
	public void test10() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(0,9)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		String string = rover.executeCommand("b");
		assertEquals("(0,0,N)(0,9)", string);
	}
	
	
	@Test
	public void test11() throws MarsRoverException {
		
		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(2,2)");
		listObstacle.add("(0,5)");
		listObstacle.add("(5,0)");
		
		MarsRover rover = new MarsRover(6, 6, listObstacle);
		
		String string = rover.executeCommand("ffrfffrbbblllfrfrbbl");
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", string);
	}
}
